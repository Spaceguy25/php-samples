<?php
/*
This is checking if the input number is even or odd.
*/

function EvenOdd ($num) {
    if ($num % 2 == 0){
        echo "$num is an even number";
    }
    else {
        echo "$num is an odd number. ";
        echo "Pick another number";
    }
}

if (isset($_POST['submit'])) {
    $num = $_POST['number'];
    EvenOdd($num);
}

?>

<html>
    <head>
        <title>EvenOdd</title>
    </head>
    <body>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label>The following program will determine if a number is even or odd</label>
            <input type="text" name="number" />
            <input type="submit" name="submit" value="submit" />
        </form>
    </body>
</html>