<?php
/**
Deck of cards. Using shuffle function.
 */

$deck = array(
        "Spades"=> array(
            "One",
            "Two",
            "Three",
            "Four",
            "Five",
            "Six",
            "Seven",
            "Eight",
            "Nine",
            "Ten",
            "Jack",
            "Queen",
            "King"
        ),
        "Hearts"=> array(
            "One",
            "Two",
            "Three",
            "Four",
            "Five",
            "Six",
            "Seven",
            "Eight",
            "Nine",
            "Ten",
            "Jack",
            "Queen",
            "King"
        ),
        "Clubs"=> array(
            "One",
            "Two",
            "Three",
            "Four",
            "Five",
            "Six",
            "Seven",
            "Eight",
            "Nine",
            "Ten",
            "Jack",
            "Queen",
            "King"
        ),
        "Diamonds"=> array(
            "One",
            "Two",
            "Three",
            "Four",
            "Five",
            "Six",
            "Seven",
            "Eight",
            "Nine",
            "Ten",
            "Jack",
            "Queen",
            "King"
        )
);

shuffling($deck);
function shuffling ($deckToPrint){
    foreach($deckToPrint as $suit => $cards){
        echo "<br>$suit<br>";
        shuffle($cards);
        foreach ($cards as $value) {
            echo "$value, ";
        }
    }

}

//print_r($deck);
//http://www.tutorialspoint.com/php/php_arrays.htm
