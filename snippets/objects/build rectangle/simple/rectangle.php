<?php
/**
Rectangle object.
 */

class rectangle {
    public $width = 0;
    public $height = 0;
    public $red;
    public $green;
    public $blue;

    function __construct($w = 20, $h = 10, $red = "0", $green = "0", $blue = "250"){
        $this->height = $h;
        $this->width = $w;
        $this->red = $red;
        $this->green = $green;
        $this->blue = $blue;
    }

    function printRectangle() {
        return '<div style="width:' . $this->width . 'px; height:' .
        $this->height . 'px; background-color: rgb(' .$this->red ."," . $this->green ."," . $this->blue . '); margin:10px;"></div>';
    }
}