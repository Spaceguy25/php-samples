<?php
/**
Depends on rectangle object.
 */
    require_once('rectangle.php');
?>

<!DOCTYPE html>

<html>
<head>
    <title>Rectangle Builder</title>
</head>
<body>
    <?php
        if (isset($_POST['submit'])) {
            if(!isset($_POST['length']) || !isset($_POST['width'])){
                $rectangle = new rectangle();
            }
            else {
                $rectangle = new rectangle(
                    $_POST['length'], $_POST['width'], $_POST['red'],
                    $_POST['green'], $_POST['blue']);
            }
            echo $rectangle->printRectangle();

        }
    ?>
    <h2>Rectangle Builder</h2>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <fieldset>
            <legend>Dimensions</legend>
            <label>Height:</label>
            <input type="number" name="length" min="10" max="300" step="10"/><br>
            <label>Width:</label>
            <input type="number" name="width" min="10" max="250" step="10"/><br>
        </fieldset>
        <fieldset>
            <legend>Color Builder</legend>
            <label>Red:</label>
            <input type="number" name="red" min="0" max="255" step="5"/><br>
            <label>Green:</label>
            <input type="number" name="green" min="0" max="255" step="5"/><br>
            <label>Blue:</label>
            <input type="number" name="blue" min="0" max="255" step="5"/><br>
        </fieldset>
        <input type="submit" name="submit" value="Make Rectangle" />
    </form>
</body>
</html>

