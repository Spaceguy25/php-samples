
<?php
/**

 */

class rectangle {
    public $width = 0;
    public $height = 0;
    public $color = "rgb(0, 0, 255)";

    function __construct($w = 20, $h = 10, $c = "rgb(0, 0, 255)"){
        $this->height = $h;
        $this->width = $w;
        $this->color = $c;
    }
	
	//Tell the object no longer exists
	function __destruct () {
		echo "<p>The rectangle no longer exists.</p>";
	}
	
	//Display error if the user tries to access a variable that doesn't exist
	function __get($name) {
		echo "<p>Error: $name is not a member variable.</p>";
	}
	
	//Display error if the user tries to access a variable that doesn't exist
	function __set($name, $value) {
		echo "<p>Error: $name is not a member variable.</p>";
	}

    function __call($name, $arguments) {
        echo "<p>Error calling $name with the following arguments: " . implode (',', $arguments) . "</p><br/>";
    }

	//Display the rectangle
	function __toString () {
		return '<div style="width: ' . $this->width . 'px; height: ' . $this->height . 'px; background-color: ' . $this->color . '; margin: 10px;"></div>';
        //return "res";
	}

    /*function printRectangle() {
        return '<div style="width:' . $this->width . 'px; height:' .
        $this->height . 'px; background-color: rgb(' .$this->red ."," . $this->green ."," . $this->blue . '); margin:10px;"></div>';
    }*/
}