<?php
/**

 */
	session_start();
    require_once('rectangle.php');
?>

<!DOCTYPE html>


<html>
<head>
    <title>Rectangle Builder</title>
</head>
<body>
    <?php
        if (isset($_POST['submit']) && !empty($_POST['height']) && !empty($_POST['width']) && !empty($_POST['red']) && !empty($_POST['green']) && !empty($_POST['blue'])) {
            if(isset($_POST['height']) || isset($_POST['width'])){
                //$rectangle = new rectangle();
                $width = $_POST['width'];
                $height = $_POST['height'];
                $red = $_POST['red'];
                $green = $_POST['green'];
                $blue = $_POST['blue'];
                $color = "rgb($red, $green, $blue)";
                echo $color;
                $rectangle = new rectangle($width, $height, $color);
                echo $rectangle;
            }
            else {
               $rectangle = new rectangle();
               echo $rectangle;
           }
            //echo $rectangle->printRectangle();

            echo "<br>__call: ";
            echo $rectangle -> printRectangle('color', ' length', ' width');
            echo "<br> __get: ";
            $rectangle -> lenght;
            echo "<br> __ set: ";
            echo $rectangle -> length = "200";
            echo "<br> __destruct: ";
            unset($rectangle);
        }


    ?>
    <h2>Rectangle Builder</h2>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <fieldset>
            <legend>Dimensions</legend>
            <label>Height:</label>
            <input type="number" name="height" min="10" max="300" step="10"/><br>
            <label>Width:</label>
            <input type="number" name="width" min="10" max="250" step="10"/><br>
        </fieldset>
        <fieldset>
            <legend>Color Builder</legend>
            <label>Red:</label>
            <input type="number" name="red" min="0" max="255" step="5"/><br>
            <label>Green:</label>
            <input type="number" name="green" min="0" max="255" step="5"/><br>
            <label>Blue:</label>
            <input type="number" name="blue" min="0" max="255" step="5"/><br>
        </fieldset>
        <input type="submit" name="submit" value="Make Rectangle" />
    </form>

</body>
</html>

