<?php
/**
This is pulling images from the database. Make note that you need access to database to make this work.
 */
require_once ('image.php');
require_once ('connectvars.php');
?>


<!DOCTYPE html>

<html>
<head>
    <title>Lab 9</title>
    <link href="main.css" rel="stylesheet" type="text/css" />
</head>
<body>
<h2>This website contains a gallery of some books great books</h2>
<?php
    $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    $query = "SELECT * FROM images";
$data = mysqli_query($dbc, $query);
while ($row = mysqli_fetch_array($data)) {
    $image = new image($row['picture'], $row['alt']);
    $image->scale(200,200);
    $image->setWatermarked(true);
    $image->displayImage();
}
?>
</body>
</html>


