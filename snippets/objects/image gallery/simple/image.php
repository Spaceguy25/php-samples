<?php
/**
Image object.
 */

class image {
    private $image;
    private $width;
    private $height;
    private $watermarked;
    private $alt;

    function __construct ($image, $alt) {
        $this -> image = $image;
        $this -> alt = $alt;
    }

    function setWatermarked($watermarked) {
        $this -> watermarked = $watermarked;
    }

    function scale($height, $width) {
        $this -> width = $width;
        $this -> height = $height;
    }

    function displayImage() {
        if ($this -> watermarked) {
            print "<p class='watermark'><img src='" . $this -> image . "' class='watermarked' width='" . $this -> width . "' height='" .
                $this -> height . "' /></p>";
        }
        else{
            print "<p class='watermark'><img src='" . $this -> image . "' width='" . $this -> width . "' height='" .
                $this -> height . "' /></p>";
        }

    }


}