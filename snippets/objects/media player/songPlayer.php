<?php

/**

 */
require_once('song.php');
?>
<!DOCTYPE html>
    <html>
        <head><title>Song Player</title></head>
        <body>
            <h2>Song Player</h2>
            <?php
                $song = new song("Symphony No. 9 II", "10:45", "Beethoven",
                    "ice-cubes-glass.mp3");
                echo '<audio controls>' .
                    '<source src="' . $song -> getAudio() . '" type="audio/mpeg">' .
                    "</audio>";
            ?>
        </body>
    </html>