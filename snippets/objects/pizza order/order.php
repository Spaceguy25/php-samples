<?php
/**
Order object sample.
 */

class order {
    private $PizzaSize;
    private $quantity;
    private $customerName;
    private $customerAddress;
    private $customerPhone;
    private $receipt;

    function __construct($PizzaSize, $quantity, $customerName, $customerAddress, $customerPhone, $receipt) {
        $this -> size = $PizzaSize;
        $this -> quantity = $quantity;
        $this -> name = $customerName;
        $this -> address = $customerAddress;
        $this -> phone = $customerPhone;
        $this -> receipt = $receipt;
    }

    function printOrder () {
        echo "Thank you $this->name, Your order has been placed
        $this->quantity x $this->size Pizza<br/>Adress: $this->address<br>
        Phone #: $this->phone<br>
        Receipt Number: $this->receipt<br/>";
    }
}