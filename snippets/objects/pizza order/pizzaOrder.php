<?php
/**
Depends on order object.
 */
    require_once('order.php');
?>
<!DOCTYPE html>

<html>
    <head>
        <title>Order Pizza</title>
    </head>
    <body>
        <?php
        if (isset($_POST['order'])) {
            $order = new order(
                $_POST['size'], $_POST['quantity'], $_POST['name'],
                $_POST['address'],$_POST['phoneNumber'], 0);
            $order->printOrder();
        }
        ?>
        <h2>Order Pizza</h2>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label><br/>Pizza Size:</label>
            <select name="size">
                <option value="small" name="small">Small</option>
                <option value="medium" name="medium">Medium</option>
                <option value="large" name="large">Large</option>
            </select>
            <label>Quantity:</label>
            <input type="number" name="quantity" /><br>
            <label>Name</label>
            <input type="text" name="name" /><br>
            <label>Phone Number:</label>
            <input type="tel" name="phoneNumber" /><br>
            <label>Address</label>
            <input type="text" name="address" /><br>
            <input type="submit" name="order" value="Place Order" />
            <input type="submit" name="clear" value="Start Over" />
        </form>
    </body>
</html>
