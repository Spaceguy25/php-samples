<?php
/**

 */

class package {
    private $from;
    private $to;
    private $weight;
    private $height;
    private $width;

    function __construct ($from, $to, $weight, $height, $width) {
        $this -> from = $from;
        $this -> to = $to;
        $this -> weight = $weight;
        $this -> height = $height;
        $this -> width = $weight;
    }
    //Magic Methods
    function __destruct () {
        echo "<p>Package no loonger present.</p>";
    }

    function __get($name) {
        echo "<p>Error: $name is not a member variable.</p>";
    }

    function __set($name, $value) {
        echo "<p>Error: $name is not a member variable and" . "can not be set to $value</p>";
    }

    function __toString(){
        return "<p>The package is being sent to: $this -> to <br>" .
        "The package is veing sent from: $this -> from <br>" .
        "The package dimensions are $this -> height cm" .
        "high x $this -> width cm wide<br>" .
        "</p>";
    }

    function __sleep () {
        echo "Sending Package";
        return array('from', 'to', 'weight', 'height', 'width');
    }

    function __wakeup () {
        echo "<p>Package Received:</p>";
    }

    function __call($name, $arguments) {
        echo "<p>Error calling $name with the following arguments:";
            implode (', ', $arguments) . "</p>";
    }
}